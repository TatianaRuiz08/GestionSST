package com.example.tatiana.gestionsst;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tatiana.gestionsst.utilidades.Utilidades;

public class NuevaEmpresa extends AppCompatActivity {

    TextView txtnombre, txtnit, txtdireccion, txttelefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_empresa);

        txtnombre = (EditText) findViewById(R.id.nombre);
        txtnit = (EditText) findViewById(R.id.nit);
        txtdireccion = (EditText) findViewById(R.id.direccion);
        txttelefono = (EditText) findViewById(R.id.telefono);
    }

    public void onClick(View v) {
        registrarEmpresa();

    }

    private void registrarEmpresa() {
        DBHelper admin = new DBHelper(this, "db_usuarios", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_NOMBRE, txtnombre.getText().toString());
        values.put(Utilidades.CAMPO_NIT, txtnit.getText().toString());
        values.put(Utilidades.CAMPO_DIRECCION, txtdireccion.getText().toString());
        values.put(Utilidades.CAMPO_TELEFONO, txttelefono.getText().toString());

        Long idResult = db.insert(Utilidades.TABLA_EMPRESA, Utilidades.CAMPO_NOMBRE, values);

        //Toast.makeText(getApplicationContext(), "Id:" + idResult, Toast.LENGTH_SHORT).show();

        if(idResult != -1){

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Registro");
            builder.setMessage("Empresa registrada correctamente");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent myIntent = new Intent(NuevaEmpresa.this, TabbedMenu.class);
                    startActivity(myIntent);

                }
            });
            builder.create();
            builder.show();

        }

    }
}
