package com.example.tatiana.gestionsst.entidades;

public class Usuario {

    private String id;
    private String documento;
    private String email;
    private String contrasena;

    public Usuario(String id, String documento, String email, String contrasena){

        this.documento = documento;
        this.email = email;
        this.contrasena = contrasena;

    }

    public String getId(){
        return id;
    }

    private void setId(String id){
        this.id = id;
    }

    public String getDocumento(){
        return documento;
    }

    private void setDocumento(String documento){
        this.documento = documento;
    }

    public String getEmail(){
        return email;

    }

    private void setEmail(String email){
        this.email = email;
    }

    public String getContrasena(){
        return contrasena;

    }

    private void setContrasena(String contrasena){
        this.contrasena = contrasena;
    }

}
