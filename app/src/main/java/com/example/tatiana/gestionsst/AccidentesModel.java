package com.example.tatiana.gestionsst;

public class AccidentesModel {

    private String titulo;
    private String descripcion;
    private int imagenId;
    private String fecha;
    private String nivel;

    public AccidentesModel(String titulo, String descripcion, int imagenId, String fecha, String nivel){

        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagenId = imagenId;
        this.fecha = fecha;
        this.nivel = nivel;

    }

    public AccidentesModel(){}

    public String getTitulo(){ return titulo;}
    public void setTitulo(String titulo) {this.titulo = titulo;}

    public String getDescripcion(){ return descripcion;}
    public void setDescripcion(String descripcion) {this.descripcion = descripcion;}

    public int getImagenId(){ return imagenId;}
    public void setImagenId(int imagenId) {this.imagenId = imagenId;}

    public String getFecha(){ return fecha;}
    public void setFecha(String fecha) {this.fecha = fecha;}

    public String getNivel(){ return nivel;}
    public void setNivel(String fecha) {this.nivel = nivel;}

}

