package com.example.tatiana.gestionsst;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class Empresa extends AppCompatActivity {

    private Cursor fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);

        DBHelper admin=new DBHelper(this,"db_usuarios",null,1);
        SQLiteDatabase db=admin.getWritableDatabase();

        fila=db.rawQuery("select * from empresa",null);

        Toast.makeText(getApplicationContext(), "Id:" + fila, Toast.LENGTH_SHORT).show();

    }
}
