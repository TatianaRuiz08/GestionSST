package com.example.tatiana.gestionsst;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tatiana.gestionsst.utilidades.Utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NuevaTarea extends AppCompatActivity {

    private TextView tvDateValue;

    private int year;
    private int month;
    private int day;
    TextView txtdescripcion, txtfecha;

    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_tarea);

        tvDateValue = (TextView) findViewById(R.id.dateValue);

        context = this;

        final Button btnOpenPopup = (Button) findViewById(R.id.btnCalendar);
        btnOpenPopup.setOnClickListener(new Button.OnClickListener(){

            @Override

            public void onClick(View arg0) {
                showDatePickerDialog(arg0);
            }
        });

        txtdescripcion = (EditText) findViewById(R.id.descripcion);
        txtfecha = (EditText) findViewById(R.id.dateValue);
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(context.getFragmentManager(), "datePicker");
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Usar del defecto la fecha actual
            final Calendar c = Calendar.getInstance();
            try {
                // Si en algun momento se ha informado la fecha se recupera
                String format = "MM-dd-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
                c.setTime(sdf.parse(String.valueOf(tvDateValue.getText())));
            } catch (ParseException e) {
                // Si falla utilizaremos la fecha actual
            }

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of TimePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            try{
                final Calendar c = Calendar.getInstance();
                c.set(year, month, day);
                String format = "MM-dd-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
                tvDateValue.setText(sdf.format(c.getTime()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        registrarTareas();

    }

    private void registrarTareas() {
        DBHelper admin = new DBHelper(this, "db_usuarios", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_TITULO, "Andres Guerrero");
        values.put(Utilidades.CAMPO_DESCRIPCION, txtdescripcion.getText().toString());
        values.put(Utilidades.CAMPO_FECHA, txtfecha.getText().toString());

        Long idResult = db.insert(Utilidades.TABLA_TAREAS, Utilidades.CAMPO_TITULO, values);

        //Toast.makeText(getApplicationContext(), "Id:" + idResult, Toast.LENGTH_SHORT).show();

        if(idResult != -1){

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Registro");
            builder.setMessage("Tarea registrada correctamente.");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent myIntent = new Intent(NuevaTarea.this, Tareas.class);
                    startActivity(myIntent);

                }
            });
            builder.create();
            builder.show();

        }

    }
}
