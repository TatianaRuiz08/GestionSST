package com.example.tatiana.gestionsst;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RiesgosAdapter extends RecyclerView.Adapter<RiesgosAdapter.ViewHolder> {

    private ArrayList<RiesgosModel> ListaRiesgos;

    public RiesgosAdapter(ArrayList<RiesgosModel> ListaRiesgos) {
        this.ListaRiesgos = ListaRiesgos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_riesgos, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtNombre.setText(ListaRiesgos.get(position).getTitulo());
        holder.txtInformacion.setText(ListaRiesgos.get(position).getDescripcion());
        holder.foto.setImageResource(ListaRiesgos.get(position).getImagenId());
    }

    @Override
    public int getItemCount() {
        return ListaRiesgos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNombre, txtInformacion;
        ImageView foto;

        public ViewHolder(View v) {
            super(v);
            txtNombre = (TextView) itemView.findViewById(R.id.idNombre);
            txtInformacion = (TextView) itemView.findViewById(R.id.idInfo);
            foto = (ImageView) itemView.findViewById(R.id.idImagen);
        }
    }

}
