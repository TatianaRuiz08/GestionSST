package com.example.tatiana.gestionsst;

public class TareasModel {

    private String titulo;
    private String descripcion;
    private int imagenId;

    public TareasModel(String titulo, String descripcion, int imagenId){

        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagenId = imagenId;

    }

    public TareasModel(){}

    public String getTitulo(){ return titulo;}
    public void setTitulo(String titulo) {this.titulo = titulo;}

    public String getDescripcion(){ return descripcion;}
    public void setDescripcion(String descripcion) {this.descripcion = descripcion;}

    public int getImagenId(){ return imagenId;}
    public void setImagenId(int imagenId) {this.imagenId = imagenId;}


}
