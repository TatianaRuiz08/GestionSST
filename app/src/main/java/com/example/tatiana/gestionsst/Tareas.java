package com.example.tatiana.gestionsst;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import com.example.tatiana.gestionsst.DBHelper;

public class Tareas extends Fragment {

    private RecyclerView recycler;
    private RecyclerView.Adapter mAdapter;
    ArrayList<TareasModel> ListaTareas;
    private RecyclerView.LayoutManager mLayoutManager;
    private DBAux dbHelper;
    private TareasAdapter adapter;

    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tareas, container, false);

        ListaTareas = new ArrayList<>();
        recycler = rootView.findViewById(R.id.reciclador);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarLista();

        TareasAdapter adapter = new TareasAdapter(ListaTareas);
        recycler.setAdapter(adapter);

        return rootView;

    }


    private void llenarLista(){
        ListaTareas.add(new TareasModel("Tatiana Ruiz", "Hacer inspección el extintor", R.drawable.nofoto));
        ListaTareas.add(new TareasModel("Alberto Cárdenas", "Hacer el formato de inspección del botiquin", R.drawable.nofoto));
        ListaTareas.add(new TareasModel("Tatiana Ruiz", "Realizar el cronograma de Actividades", R.drawable.nofoto));
        ListaTareas.add(new TareasModel("Cristian Camargo", "Realizar Inspección del Extintor", R.drawable.nofoto));
        ListaTareas.add(new TareasModel("Tatiana Ruiz", "Realizar inspección a puestos de Trabajo", R.drawable.nofoto));
    }


}


