package com.example.tatiana.gestionsst;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.DialogInterface;

import com.example.tatiana.gestionsst.utilidades.Utilidades;

public class RegistroPersona extends AppCompatActivity {

    Button btnLogin;
    TextView txtdocumento, txtemail, txtcontrasena;
    private Cursor fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_persona);

        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btnLogin = new Intent(RegistroPersona.this, Login.class);
                startActivity(btnLogin);
            }
        });

        txtdocumento = (EditText) findViewById(R.id.documento);
        txtemail = (EditText) findViewById(R.id.email);
        txtcontrasena = (EditText) findViewById(R.id.password);
    }

    public void onClick(View v) {
        registrarUsuarios();

    }

    private void registrarUsuarios() {
        DBHelper admin = new DBHelper(this, "db_usuarios", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_DOCUMENTO, txtdocumento.getText().toString());
        values.put(Utilidades.CAMPO_EMAIL, txtemail.getText().toString());
        values.put(Utilidades.CAMPO_CONTRASENA, txtcontrasena.getText().toString());

        Long idResult = db.insert(Utilidades.TABLA_USUARIO, Utilidades.CAMPO_DOCUMENTO, values);

        if(idResult == 1){

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Registro");
            builder.setMessage("Registrado correctamente");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent myIntent = new Intent(RegistroPersona.this, Login.class);
                    startActivity(myIntent);

                }
            });
            builder.create();
            builder.show();

        }

    }

}