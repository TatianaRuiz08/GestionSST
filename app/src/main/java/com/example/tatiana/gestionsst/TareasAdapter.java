package com.example.tatiana.gestionsst;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TareasAdapter extends RecyclerView.Adapter<TareasAdapter.ViewHolder> {

    private ArrayList<TareasModel> ListaTareas;

    public TareasAdapter(ArrayList<TareasModel> ListaTareas) {
        this.ListaTareas = ListaTareas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_columna, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtNombre.setText(ListaTareas.get(position).getTitulo());
        holder.txtInformacion.setText(ListaTareas.get(position).getDescripcion());
        holder.foto.setImageResource(ListaTareas.get(position).getImagenId());
    }

    @Override
    public int getItemCount() {
        return ListaTareas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNombre, txtInformacion;
        ImageView foto;

        public ViewHolder(View v) {
            super(v);
            txtNombre = (TextView) itemView.findViewById(R.id.idNombre);
            txtInformacion = (TextView) itemView.findViewById(R.id.idInfo);
            foto = (ImageView) itemView.findViewById(R.id.idImagen);
        }
    }

}
