package com.example.tatiana.gestionsst;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.database.Cursor;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    Button btnRegistro;
    EditText txtusuario, txtcontrasena;
    private Cursor fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtusuario = (EditText) findViewById(R.id.email);
        txtcontrasena = (EditText) findViewById(R.id.password);
        btnRegistro = (Button)findViewById(R.id.botonRegistro);

        btnRegistro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent btnRegistro = new Intent(Login.this, RegistroPersona.class);
                startActivity(btnRegistro);
            }
        });
    }

    public void ingresar(View v){
        DBHelper admin=new DBHelper(this,"db_usuarios",null,1);
        SQLiteDatabase db=admin.getWritableDatabase();

        String usuario=txtusuario.getText().toString();
        String contrasena=txtcontrasena.getText().toString();

        fila=db.rawQuery("select email,contrasena from usuarios where email='"+usuario+"' and contrasena='"+contrasena+"'",null);

        if(fila.moveToFirst()==true){

            String user = fila.getString(0);
            String pass = fila.getString(1);

            if (usuario.equals(user)&&contrasena.equals(pass)){

                Intent ven=new Intent(this,NuevaEmpresa.class);
                startActivity(ven);

                txtusuario.setText("");
                txtcontrasena.setText("");

            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Title");
                builder.setMessage("Nada correcto");
                builder.setPositiveButton("Ok", null);
                builder.create();
                builder.show();
            }
        }

    }
}
