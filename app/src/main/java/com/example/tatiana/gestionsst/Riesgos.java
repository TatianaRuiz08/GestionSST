package com.example.tatiana.gestionsst;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class Riesgos extends Fragment {

    RecyclerView recycler;
    private RecyclerView.Adapter mAdapter;
    ArrayList<RiesgosModel> ListaRiesgos;

    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.riesgos, container, false);

        ListaRiesgos = new ArrayList<>();
        recycler = rootView.findViewById(R.id.recicladorRiesgos);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarLista();

        RiesgosAdapter adapter = new RiesgosAdapter(ListaRiesgos);
        recycler.setAdapter(adapter);

        return rootView;
    }

    private void llenarLista(){
        ListaRiesgos.add(new RiesgosModel("Daniel Stevan", "La pantalla de mi puesto de trabajo se dañó", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaRiesgos.add(new RiesgosModel("Tatiana Ruiz", "No hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaRiesgos.add(new RiesgosModel("Tatiana Ruiz", "Hay mal olor en las bodegas", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaRiesgos.add(new RiesgosModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaRiesgos.add(new RiesgosModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
    }
}
