package com.example.tatiana.gestionsst;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.tatiana.gestionsst.utilidades.Utilidades;

public class DBHelper extends SQLiteOpenHelper{

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(Utilidades.CREAR_TABLA_USUARIO);
        db.execSQL(Utilidades.CREAR_TABLA_TAREAS);
        db.execSQL("INSERT INTO "+Utilidades.TABLA_TAREAS+" values " +
                "('Hacer inspección el extintor', 'Mes de Julio', '27 Julio'), " +
                "('Inpección del Botiquin', 'Hacer el formato de inspección del botiquin', '27 Julio'), " +
                "('Realizar el cronograma de Actividades', 'desde la fecha hasta Octubre', '20 Julio') ");
        db.execSQL(Utilidades.CREAR_TABLA_EMPRESA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS usuarios");
        db.execSQL("DROP TABLE IF EXISTS tareas");
        db.execSQL("DROP TABLE IF EXISTS empresa");
        onCreate(db);
    }
}
