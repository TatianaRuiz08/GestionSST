package com.example.tatiana.gestionsst;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import java.util.ArrayList;

public class Accidentes extends Fragment {

    RecyclerView recycler;
    private RecyclerView.Adapter mAdapter;
    ArrayList<AccidentesModel> ListaAccidentes;

    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.accidentes, container, false);

        ListaAccidentes = new ArrayList<>();
        recycler = rootView.findViewById(R.id.recicladorAccidentes);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarAccidentes();

        AccidentesAdapter adapter = new AccidentesAdapter(ListaAccidentes);
        recycler.setAdapter(adapter);

        return rootView;
    }

    private void llenarAccidentes(){
        ListaAccidentes.add(new AccidentesModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaAccidentes.add(new AccidentesModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaAccidentes.add(new AccidentesModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaAccidentes.add(new AccidentesModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
        ListaAccidentes.add(new AccidentesModel("Tatiana Ruiz", "Resbalé en las escaleras, no hay cintas antideslizantes", R.drawable.nofoto, "21 Ago", "Medio"));
    }

}
