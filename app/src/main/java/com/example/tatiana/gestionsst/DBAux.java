package com.example.tatiana.gestionsst;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.tatiana.gestionsst.utilidades.Utilidades;

import java.util.ArrayList;
import java.util.List;

public class DBAux extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "db_tareas";
    private static final int DATABASE_VERSION = 1;

    public DBAux(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utilidades.CREAR_TABLA_TAREAS);
        db.execSQL("INSERT INTO " + Utilidades.TABLA_TAREAS + " values " +
                "('Hacer inspección el extintor', 'Mes de Julio', '27 Julio'), " +
                "('Inpección del Botiquin', 'Hacer el formato de inspección del botiquin', 'nofoto'), " +
                "('Realizar el cronograma de Actividades', 'desde la fecha hasta Octubre', 'nofoto') ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tareas");
        onCreate(db);
    }

    public List<TareasModel> petList(String filtro) {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Utilidades.TABLA_TAREAS, null);
        TareasModel tareas;

        List<TareasModel> listTareas = new ArrayList<>();

        while (cursor.moveToNext()) {

            tareas = new TareasModel();
            tareas.setTitulo(cursor.getString(0));
            tareas.setDescripcion(cursor.getString(1));
            tareas.setImagenId(cursor.getInt(2));

            listTareas.add(tareas);
        }

        return listTareas;
    }
}
