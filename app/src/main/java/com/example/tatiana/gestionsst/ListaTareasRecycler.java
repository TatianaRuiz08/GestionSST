package com.example.tatiana.gestionsst;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.tatiana.gestionsst.utilidades.Utilidades;

import java.util.ArrayList;

public class ListaTareasRecycler extends AppCompatActivity {

    ArrayList<TareasModel> listTareas;
    RecyclerView recyclerViewUsuarios;

    DBHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tareas);

        conn = new DBHelper(getApplicationContext(), "bd_usuarios", null, 1);
        listTareas = new ArrayList<>();

        recyclerViewUsuarios = (RecyclerView) findViewById(R.id.reciclador);
        recyclerViewUsuarios.setLayoutManager(new LinearLayoutManager(this));

        ConsultarListaTareas();

    }

    private void ConsultarListaTareas(){
        SQLiteDatabase db = conn.getReadableDatabase();

        TareasModel tareas = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_TAREAS, null);

        while(cursor.moveToNext()){

            tareas = new TareasModel();
            tareas.setTitulo(cursor.getString(0));
            tareas.setDescripcion(cursor.getString(1));
            tareas.setImagenId(cursor.getInt(2));

            listTareas.add(tareas);
        }

    }
}
