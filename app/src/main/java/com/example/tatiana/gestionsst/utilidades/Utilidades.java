package com.example.tatiana.gestionsst.utilidades;

public class Utilidades {

    /* Propiedades Usuarios*/
    public static final String TABLA_USUARIO = "usuarios";
    public static final String CAMPO_DOCUMENTO = "documento";
    public static final String CAMPO_EMAIL = "email";
    public static final String CAMPO_CONTRASENA = "contrasena";


    /* Propiedades Tareas */
    public static final String TABLA_TAREAS = "tareas";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    public static final String CAMPO_FECHA = "fecha";

    /* Propiedades Accidentes */
    public static final String TABLA_ACCIDENTES = "accidentes";
    public static final String CAMPO_NIVEL = "descripcion";

    /* Propiedades Empresa*/
    public static final String TABLA_EMPRESA = "empresa";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_NIT = "nit";
    public static final String CAMPO_DIRECCION = "direccion";
    public static final String CAMPO_TELEFONO = "telefono";


    /* Tabla Usuarios */
    public static final String CREAR_TABLA_USUARIO="CREATE TABLE "+TABLA_USUARIO+" ("+CAMPO_DOCUMENTO+" text, "+CAMPO_EMAIL+" text, "+CAMPO_CONTRASENA+" text)";

    /* Tabla Tareas */
    public static final String CREAR_TABLA_TAREAS="CREATE TABLE "+TABLA_TAREAS+" ("+CAMPO_TITULO+" text, "+CAMPO_DESCRIPCION+" text, "+CAMPO_FECHA+" text)";

    /* Tabla Accidentes */
    public static final String CREAR_TABLA_ACCIDENTES="CREATE TABLE "+TABLA_ACCIDENTES+" ("+CAMPO_TITULO+" text, "+CAMPO_DESCRIPCION+" text, "+CAMPO_NIVEL+" text, "+CAMPO_FECHA+" text)";

    /* Tabla Empresa */
        public static final String CREAR_TABLA_EMPRESA="CREATE TABLE "+TABLA_EMPRESA+" ("+CAMPO_NOMBRE+" text, "+CAMPO_NIT+" text, "+CAMPO_DIRECCION+" text, "+CAMPO_TELEFONO+" text)";

}
